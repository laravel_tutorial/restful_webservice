<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;

class Board extends Model
{
    protected $fillable = [
        'id',
        'name', 
        'totol_article_in_board',
        'status'
    ];

    // create relationship
    public function articles(){
        return $this->hasMany(Article::class,'board_id');
    }

}
