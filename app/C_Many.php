<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\C_One;

class C_Many extends Model
{
    protected $fillable=[
        'id',
        'title',
        'description',
        'c_one_id'
    ];

    public function COne(){
        return $this->belongsTo(C_One::class);
    }
}
