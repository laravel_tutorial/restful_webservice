<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Board;

class Article extends Model
{
    protected $fillable = [
        'id',
        'title',
        'body'
    ];

    // create relationship
    public function board(){
        return $this->belongsTo(Board::class);
    }


}
