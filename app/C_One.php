<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\C_Many;

class C_One extends Model
{
    protected $fillable=[
        'id',
        'name'
    ];

    public function CMany(){
        return $this->hasMany(C_Many::class,'c_one_id');
    }

}
